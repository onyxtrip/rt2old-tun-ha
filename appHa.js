const express = require('express')
const app = express()
const shell = require('shelljs')
const cron = require('node-cron');
const cronOne = require('node-cron');

const cronSpeed = require('node-cron');
const shellExec = require('shell-exec')
var decimalPointRegex = /\-?\d+\.\d+/g;
let fs = require('fs')
let port = 443
let ha='global\n' +
    '    maxconn           600000\n' +
    '    nbproc            6\n' +
    '\n' +
    'frontend www\n' +
    '\n' +
    '    bind              :443\n' +
    '\n' +
    '    mode              tcp\n' +
    '\n' +
    '    default_backend   bencane.com\n' +
    '\n' +
    'backend bencane.com\n' +
    '\n' +
    '    mode              tcp\n' +
    '\n' +
    '    balance           roundrobin\n' +
    '\n'


let countReboot = 4
let tryFail = 0

app.get('/', (req, res) => {
    res.send('Hello World!')
    run()
})

app.get('/ip/:ip/', (req, res) => {
    // console.log(req.params.acc)
    //return addNewIp(req, res)

})

app.get('/rst2', (req, res) => {

    return reboot()

})

app.get('/test', (req, res) => {
    // console.log(req.params.acc)
    res.sendfile('jetmtp.png')

})

async function addNewIp(req, res) {
    res.send('Hello World!')
    console.log(req.params.ip)
    addBaship(req.params.ip)
}

async function addBaship(ip) {
    return await shell.exec(' ip addr add ' + ip + ' dev eth0').stderr;

}


async function reboot() {
    return await shell.exec(' sudo reboot ').stderr;

}

app.listen(3000, () => console.log(`Example app listening on port 3000!`))
run()

async function run() {
    let data = await getTunProxy()
    let s=''
    for (let i=0;i<data.length;i++)
    {
        let sample='    server            s'+i+' '+data[i].ip+':443 check  \n'
        s=s+sample
    }
    let tunProxyIp = data.ip
    let temp = ha+s
    //  console.log(temp)

    let d= await createfile(temp)
   // console.log('hiii')
    //  console.log(d)
    let st='"'+temp +'"'
    let a = await shell.exec('sudo echo '+st +' > /etc/haproxy/haproxy.cfg').stderr;
        await shell.exec('sudo systemctl restart haproxy').stderr;


}

function createfile(data) {
    return new Promise(function (resolve, reject) {
        fs.writeFile("/home/ubuntu/haproxy.cfg", data, 'utf8', function (err) {
            if (err) reject(err);
            else resolve(data);
        });
    });
}


async function doIptable(whiteIp) {
    let command = ''
    for (let i = 0; i < whiteIp.length; i++) {
        command += 'iptables -A INPUT -p tcp -s ' + whiteIp[i] + ' --dport 3000 -j ACCEPT'
        command += '\n'


    }

    command += 'iptables -A INPUT -p tcp -s 0.0.0.0/0 --dport 3000 -j DROP'
    command += '\n'

    await createfile(command)
    await execLast()
    return true
}

function createfile(command) {
    return new Promise(function (resolve, reject) {
        fs.writeFile("iptable.sh", command, 'utf8', function (err) {
            if (err) reject(err);
            else resolve(command);
        });
    });
}

async function execLast() {
    let d = await shellExec('sudo ./iptable.sh')
    return true
}

async function checkProcessWork() {
    let a = await shell.exec('nc -vz 127.0.0.1 ' + port).stderr;
    if (a.includes('succeeded') || a.includes('Connected')) {
        tryFail = 0
        return true

    }
    tryFail++
    if (tryFail >= countReboot)

        reboot()


    run()
}

function randomInt(low, high) {
    return Math.floor(Math.random() * (high - low) + low)
}

cron.schedule('*/20 * * * * *', () => {

    // checkProcessWork()
});


async function calculateSpeed() {


    let d = await shellExec('vnstat -tr')
    d = d.stdout

    let arr = d.match(decimalPointRegex)


    if (d.includes('kbit/s')) {
        arr[0] = 1
        arr[1] = 1
    } else if (d.includes('Mbit/s')) {

    } else if (d.includes('Gbit/s')) {
        arr[0] = arr[0] * 1000
        arr[1] = arr[1] * 1000
    } else if (d.includes('bit/s')) {
        arr[0] = 1
        arr[1] = 1
    }


    await sendRate(arr[0], arr[1])


}


function sendRate(rx, tx) {
    const request = require('request');
    let url = 'http://23.88.33.146:3000/rateWithPort/' + rx + '/' + tx + '/' + port
    return new Promise(function (resolve, reject) {
        request(url, {timeout: 6000}, function (error, res, body) {
            resolve(true)

        });
    });

}


function getPort() {
    const request = require('request');
    let url = 'http://23.88.33.146:3000/amzPort'
    return new Promise(function (resolve, reject) {
        request(url, {timeout: 15000}, function (error, res, body) {
            resolve(body)

        });
    });

}

function getGenerationStatus() {
    const request = require('request');
    let url = 'http://23.88.33.146:3000/generationStatus'
    return new Promise(function (resolve, reject) {
        request(url, {timeout: 15000}, function (error, res, body) {
            resolve(body)

        });
    });

}

function getWhiteIp() {
    const request = require('request');
    let url = 'http://23.88.33.146:3000/getWhiteIp'
    return new Promise(function (resolve, reject) {
        request(url, {timeout: 15000}, function (error, res, body) {
            resolve(JSON.parse(body))

        });
    });

}

function getTunProxy() {
    const request = require('request');
    let url = 'http://admin.fcfglobal.co:3000/getTunsAll'
    return new Promise(function (resolve, reject) {
        request(url, {timeout: 15000}, function (error, res, body) {
            resolve(JSON.parse(body))

        });
    });

}


cronSpeed.schedule(' */5 * * * * *', () => {

    return calculateSpeed()

});
